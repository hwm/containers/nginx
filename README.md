# containers/nginx

Un contenedor de nginx (basado en [este](https://github.com/lunatic-cat/docker-nginx-brotli)) con ngx_brotli.

Para cargar el modulo:
```
load_module /usr/local/nginx/modules/ngx_http_brotli_filter_module.so;
load_module /usr/local/nginx/modules/ngx_http_brotli_static_module.so;
```
